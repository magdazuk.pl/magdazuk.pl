var path = require("path");
var HtmlwebpackPlugin = require("html-webpack-plugin");
var webpack = require("webpack");
var merge = require("webpack-merge");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require("copy-webpack-plugin");

const TARGET = process.env.npm_lifecycle_event;
const PATHS = {
    app: path.join(__dirname, "app"),
    build: path.join(__dirname, "build")
};

var common = {
    devtool: "cheap-module-source-map",
    entry: PATHS.app,
    output: {
        publicPath: "/",
        filename: "bundle.js",
    },
    resolve: {
        extensions: ["", ".js", ".jsx"]
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loaders: ["style", "css", "sass"],
                include: PATHS.app
            },
            {
                test: /\.css$/,
                loaders: ["style", "css"],
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: ["file-loader?name=/img/[name].[ext]", "url-loader"],
            },
            {
                test: /\.jsx?$/,
                loaders: ["babel"],
                plugins: ["es7.classProperties", "es7.decorators", "es7.objectRestSpread"],
                include: PATHS.app
            },
            {test: /\.svg$/, loader: "url?limit=65000&mimetype=image/svg+xml&name=public/fonts/[name].[ext]"},
            {test: /\.woff$/, loader: "url?limit=65000&mimetype=application/font-woff&name=public/fonts/[name].[ext]"},
            {test: /\.woff2$/, loader: "url?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]"},
            {test: /\.[ot]tf$/, loader: "url?limit=65000&mimetype=application/octet-stream&name=public/fonts/[name].[ext]"},
            {test: /\.eot$/, loader: "url?limit=65000&mimetype=application/vnd.ms-fontobject&name=public/fonts/[name].[ext]"},
        ]
    },
    plugins: [
new webpack.ProvidePlugin({
   $: "jquery",
   jQuery: "jquery"
  }),
        new HtmlwebpackPlugin({
            template: path.join(PATHS.app, "index.template.html"),
            title: "Magdalena Żuk",
        }),
        new webpack.OldWatchingPlugin(),
        new CopyWebpackPlugin([
            {from: "app/img", to: "img"},
            {from: "app/favicon.ico"}
        ])
    ]
};

var deploy = {
    devtool: "cheap-module-source-map",
    entry: PATHS.app,
    output: {
        path: "./dist",
        filename: "bundle.js",
    },
    resolve: {
        extensions: ["", ".js", ".jsx"]
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract(
                "style",
                ["css", "sass"])
            },
            {
                test: /\.css$/,
                loaders: ["style", "css"],
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: ["file-loader?name=/img/[name].[ext]", "url-loader"],
            },
            {test: /\.svg$/, loader: "url?limit=65000&mimetype=image/svg+xml&name=public/fonts/[name].[ext]"},
            {test: /\.woff$/, loader: "url?limit=65000&mimetype=application/font-woff&name=public/fonts/[name].[ext]"},
            {test: /\.woff2$/, loader: "url?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]"},
            {test: /\.[ot]tf$/, loader: "url?limit=65000&mimetype=application/octet-stream&name=public/fonts/[name].[ext]"},
            {test: /\.eot$/, loader: "url?limit=65000&mimetype=application/vnd.ms-fontobject&name=public/fonts/[name].[ext]"},
            {
                test: /\.jsx?$/,
                loaders: ["babel"],
                plugins: ["es7.classProperties", "es7.decorators", "es7.objectRestSpread"],
                include: PATHS.app
            }
        ]
    },
    plugins: [
new webpack.ProvidePlugin({
   $: "jquery",
   jQuery: "jquery"
  }),
        new HtmlwebpackPlugin({
            template: path.join(PATHS.app, "index.template.html"),
            title: "Magdalena Żuk",
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true
            },
            output: {
                comments: false,
            },
            minimize: true,
        }),
        new webpack.optimize.DedupePlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify("production")
            }
        }),
        new ExtractTextPlugin("style.css"),
        new CopyWebpackPlugin([
            {from: "app/img", to: "img"},
            {from: "app/favicon.ico"}
        ])
    ]
};

if (TARGET === "start" || !TARGET) {
    module.exports = merge(common, {
        devtool: "cheap-module-source-map",
        devServer: {
            historyApiFallback: {
                disableDotRule: true
            },
            hot: true,
            inline: true,
            progress: true,

            // display only errors to reduce the amount of output
            stats: "errors-only",

            // parse host and port from env so this is easy
            // to customize
            host: process.env.HOST,
            port: process.env.PORT
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin()
        ]
    });
}
else if (TARGET === "deploy") {
    module.exports = merge(deploy, {
        devtool: "cheap-module-source-map"
    });
}
