import React from "react";
import {Link} from "react-router";
import {Col, Row, Tabs, Tab} from "react-bootstrap";

export default class Timeline extends React.Component {
    render() {
        return (
            <div>
                <h2>Timeline</h2>
                <blockquote>
                Ważne! Poniższy timeline jest odzwierciedleniem bieżącej wiedzy nas wszystkich, zaangażowanych we wsparcie rozwikłania zagadki śmierci Magdy, na podstawie dostępnych źródeł w internecie.
                Nie można traktować tej listy jako oficjalnej i z pewnością zgodnej z prawdą, choć do tego dążymy.
                </blockquote>
                <div style={{width: "100%", textAlign: "right"}}>
                <a href="https://www.wykop.pl/wpis/23979533/chyba-nie-bylo-do-weryfikacji-z-fb-bylo-w-jednym-z/">źródło</a>
                </div>
                <div className="container_"> 
                  <div className="timeline">
                    <ul>
                      <li>
                        <div className="bullet pink"></div>
                        <div className="time">25.04</div>
                        <div className="desc">
                          <h3>Wtorek</h3>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">16:05</div>
                        <div className="desc">
                          <h3>Ogłoszenie Markusa na FB: sprzedam bilety</h3>
                          <h4>Markus wystawia ogłoszenie sprzedaży biletów przynajmniej na dwóch swoich kontach fb. <a href="http://www.tygodniknarodowy.pl/6520/rekonstrukcja-ostatnich-dni-zycia-magdaleny-zuk-/">LINK</a></h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">20:25</div>
                        <div className="desc">
                          <h3>przelot Magdy KTW &rarr; RMF</h3>
                          <h4>wylot z Katowic (KTW); Magdę do Katowic odwozi być może Markus, możliwe że jest ze Złotym. Na lotnisku jest w towarzystwie dwóch mężczyzn. <a href="http://www.tvp.info/30446642/magda-zuk-do-egiptu-wyruszyla-z-pyrzowic-prokuratura-bada-nagrania">DOWÓD</a></h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet pink"></div>
                        <div className="time">26.04</div>
                        <div className="desc">
                          <h3>Środa</h3>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">00:50</div>
                        <div className="desc">
                          <h3>Przylot do Marsa Alam (RMF)</h3>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">02:45-03:30</div>
                        <div className="desc">
                          <h3>Check in?</h3>
                          <h4>wtedy powinien się odbyć checkin w hotelu The Three Corners Equinox Beach Resort</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">03:00</div>
                        <div className="desc">
                          <h3>1 telefon do Markusa</h3>
                          <h4>zaraz po zameldowaniu? - telefon do Markusa ("ktoś chodzi po moim pokoju")</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">?</div>
                        <div className="desc">
                          <h3>Kolejne dziwne telefony/SMSy</h3>
                          <h4>telefon/dziwne smsy i mmsy do Marcusa, który podobno dopiero dalej przekazuje je siostrze ("kiedy będziecie? czekam w pokoju"),</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">?</div>
                        <div className="desc">
                          <h3>Telefony do znajomych i Markusa</h3>
                          <h4>telefon do znajomych i Markusa ("przylećcie po mnie jak najszybciej i zabierzcie z Egiptu")</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">?</div>
                        <div className="desc">
                          <h3>Ambasada RP powiadomiona</h3>
                          <h4>powiadomiona Ambasada RP w Kairze nie reaguje</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">?</div>
                        <div className="desc">
                          <h3>Złoty jedzie do Pragi</h3>
                          <h4>dzień - Złoty wyjeżdża do Pragi (do weryfikacji)</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet pink"></div>
                        <div className="time">27.04</div>
                        <div className="desc">
                          <h3>Czwartek</h3>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">?</div>
                        <div className="desc">
                          <h3>Skrócenie pobytu przez biuro</h3>
                          <h4>dzień - obsługa hotelu proponuje Magdzie kontakt z lekarzem, ale ta odmawia; powiadomione biuro podróży skraca pobyt do soboty</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet pink"></div>
                        <div className="time">28.04</div>
                        <div className="desc">
                          <h3>Piątek</h3>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">rano</div>
                        <div className="desc">
                          <h3>Magda w progu pokoju hotelowego</h3>
                          <h4>Magda znaleziona w progu swojego pokoju (film i zdjęcie w szlafroku). Dowód: <a href="https://www.youtube.com/watch?v=upMSgP2UWmg">klik</a></h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">14:51 - 15:19</div>
                        <div className="desc">
                          <h3>Zarejestrowana spokojna rozmowa tel. Magdy</h3>
                          <h4>film ze szpitala w Port Ghalib (5 km od lotniska) : Magda jest spokojna, rozmawia przez telefon (z kim?) i sama swobodnie idzie</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet pink"></div>
                        <div className="time">29.04</div>
                        <div className="desc">
                          <h3>Sobota</h3>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">06:01</div>
                        <div className="desc">
                          <h3>Ostatnie logowanie do Vibera</h3>
                          <h4><a href="img/ostatnie-logowanie.jpg">DOWÓD</a></h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">ok. 14:00-15:00</div>
                        <div className="desc">
                          <h3>Próba powrotu do Polski</h3>
                          <h4>planowany odlot do Polski, Magda dociera na lotnisko, ale już nie na pokład samolotu, świadkowie widzą ją w towarzystwie czterech mężczyzn</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">ok. 16:00 - 17:30</div>
                        <div className="desc">
                          <h3>Rozmowa z Markusem</h3>
                          <h4>rozmowa wideo z Markusem (nie mogła być później, bo zachód tego dnia był o 18:11), Magda jest już roztrzęsiona, dzwoni obecny w Pradze Złoty</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">18:58</div>
                        <div className="desc">
                          <h3>Magda wjeżdża na wózku do szpitala</h3>
                          <h4>ponownie film ze szpitala w Port Ghalib: Magda wjeżdża na wózku</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">22:28</div>
                        <div className="desc">
                          <h3>Złoty w Pradze</h3>
                          <h4>Złoty oznacza się wraz ze zdjęciem na fb: "w: Prague Airport" (jest akurat 3h przed wylotem) <a href="img/zloty-praga.jpg">DOWÓD</a></h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">23:49</div>
                        <div className="desc">
                          <h3>Przepychanki na korytarzu</h3>
                          <h4>film ze szpitala - przepychanki na korytarzu A. <a href="https://www.youtube.com/watch?v=WXAQSFoGXgU">LINK</a></h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet pink"></div>
                        <div className="time">30.04</div>
                        <div className="desc">
                          <h3>Niedziela</h3>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">00:11</div>
                        <div className="desc">
                          <h3>Bójka w szpitalu</h3>
                          <h4>film ze szpitala - bójka na korytarzu B</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">01:30</div>
                        <div className="desc">
                          <h3>Złoty wylatuje z Pragi</h3>
                          <h4>przypuszczalnie Złoty wylatuje z Pragi (PRG) do Marsa Alam (RMF)</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">02:40</div>
                        <div className="desc">
                          <h3>Skok z okna</h3>
                          <h4>Magda prawdopodobnie skacze z okna</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">02:46</div>
                        <div className="desc">
                          <h3>Magda wieziona w pośpiechu wgłąb szpitala</h3>
                          <h4>Magda z zawiniętą nogą, bez przykrycia i pasów wieziona w pośpiechu wgłąb szpitala (bezpośrednio wypadku)</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">04:55</div>
                        <div className="desc">
                          <h3>Magda przykryta prześcieradłem na wózku</h3>
                          <h4>Magda przypięta pasami i przykryta prześcieradłem wieziona na łóżku</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">04:55</div>
                        <div className="desc">
                          <h3>Magda przygotowywana do transportu do Hurghady</h3>
                          <h4>łóżko z Magdą wnoszone do karetki, Magda jedzie do szpitala Red Sea w Hurghadzie, 220 km, min. 3h drogi)</h4>
                        </div>
                      </li>

                      <li>
                        <div className="bullet green"></div>
                        <div className="time">ok. 06:00</div>
                        <div className="desc">
                          <h3>Złoty w Marsa Alam</h3>
                          <h4>domniemany Maciek, a przypuszczalnie Złoty ląduje w Marsa Alam </h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">wczesny ranek</div>
                        <div className="desc">
                          <h3>Magda w szpitalu Hurghada</h3>
                          <h4>"wczesny ranek" - według dostępnego raportu medycznego wtedy Magda została przywieziona do szpitala Red Sea w Hurghadzie (w rzeczywistości powinna to być godzina nie wcześniejsza, niż ok. 08:00-08:30)</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">17:30</div>
                        <div className="desc">
                          <h3>Zgon</h3>
                          <h4>według dostępnego raportu medycznego wtedy nastąpił zgon Magdy</h4>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">23:29</div>
                        <div className="desc">
                          <h3>Złoty publikuje wpis kondolencyjny</h3>
                          <h4>Złoty dodaje wpis kondolencyjny na swoim profilu fb, który później usuwa. Dowód: <a href="img/zloty-1.jpg">klik</a></h4>
                        </div>
                      </li>

                    </ul>
                  </div>  
                </div>
            </div>
        );
    }
}

Timeline.propTypes = {
};
