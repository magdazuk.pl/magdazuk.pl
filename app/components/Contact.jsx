import React from "react";
import {Link} from "react-router";
import {Col, Row, Tabs, Tab} from "react-bootstrap";

export default class Contact extends React.Component {
    render() {
        return (
            <div>
                <h2>Organy</h2>
                <pre>Gdzie zgłaszać informacje dotyczące Magdaleny Żuk?<br />
                Kontakt do zainteresowanych tą sprawą podmiotów:<br />

                <br />
                Prokuratura Okręgowa w Jeleniej Górze<br />
                (ta która wszczęła dochodzenie w sprawie śmierci Magdaleny Żuk)<br />
                Strona: http://jgora.po.gov.pl/<br />
                e-mail: sekretariat@jgora.po.gov.pl<br />

                <br />
                Jednostki nadrzędne<br />
                (prokuratury które są nad prokuraturą Okręgową w Jeleniej Górze)<br />

                <br />
                Prokuratura Regionalna we Wrocławiu<br />
                Strona: http://www.wroclaw.pr.gov.pl/<br />
                e-mail: sekretariat@wroclaw.pr.gov.pl<br />

                <br />
                Prokuratura Krajowa (Warszawa)<br />
                Strona: http://www.pk.gov.pl/<br />
                e-mail: biuro.podawcze@pk.gov.pl<br />
                <br />
                Policja<br />
                Strona: http://www.policja.pl/<br />
                e-mail: kancelaria.gabinetkgp@policja.gov.pl<br />
                Facebook: https://www.facebook.com/PolicjaPL<br />
                Twitter: https://twitter.com/PolskaPolicja<br />
                </pre>
            </div>
        );
    }
}

Contact.propTypes = {
};
