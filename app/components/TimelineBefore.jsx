import React from "react";
import {Link} from "react-router";
import {Col, Row, Tabs, Tab} from "react-bootstrap";

export default class TimelineBefore extends React.Component {
    render() {
        return (
            <div>
                <h2>Timeline przed wyjazdem</h2>
                <div className="container_"> 
                  <div className="timeline">
                    <ul>
                      <li>
                        <div className="bullet pink"></div>
                        <div className="time">03.03</div>
                        <div className="desc">
                          <h3>Sobota</h3>
                        </div>
                      </li>
                      <li>
                        <div className="bullet green"></div>
                        <div className="time">12:55</div>
                        <div className="desc">
                          <h3>Ogłoszenie o sprzedaż mebli</h3>
                          <h4>Kwota: 4k. <a href="img/ogloszenia.jpg">DOWÓD</a></h4>
                        </div>
                      </li>
                    </ul>
                  </div>  
                </div>
            </div>
        );
    }
}

TimelineBefore.propTypes = {
};
