import React from "react";
import {Link} from "react-router";
import {Col, Row, Tabs, Tab} from "react-bootstrap";

export default class Main extends React.Component {
    render() {
        return (
            <div>
                <h3>Linki:</h3>
                <ul>
                <li><a href="https://youtu.be/UHF1iZTfj8I">wywiad z lekarzem, arabski</a> (<a href="wywiad-lekarz.mp4">backup</a>)</li>
                <li><a href="https://youtu.be/2AO0NqPpQpk">wywiad z pielęgniarką, arabski</a> (<a href="wywiad-pielegniarka.mp4">backup</a>)</li>
                <li>ucieczka Magdy ze szpitala (różne kamery i kąty nagrywania): <a href="https://www.youtube.com/watch?v=dqEnQqwTVv8">1</a>, <a href="https://www.youtube.com/watch?v=iDd8P0BW9fY">2</a>, <a href="https://www.youtube.com/watch?v=WXAQSFoGXgU">3</a>, <a href="https://www.youtube.com/watch?v=eEmxR077c1I">4</a>, <a href="https://youtu.be/gPBnsSIYj0M?t=40">5</a>, <a href="https://www.youtube.com/watch?v=EFmzL1w6GI0">6</a>, <a href="https://www.youtube.com/watch?v=Md4CM8sGGb0">7</a></li>
                <li><a href="https://www.youtube.com/watch?v=RIIp9rJ7yh4">Beautiful Girl Raped And Killed On Holidays In Egypt (Magdalena Żuk)</a></li>
                <li><a href="https://www.youtube.com/watch?v=3_8NMCWNXK0">arabskie, Here the capital Kamel Abu Ali: The incident of the Polish girl took place a week ago and we must have a crisis committee</a></li>
                <li>Specjaliści ze szpitala Port Ghalib: <a href="http://www.portghalibhospital.com/site/services_physicians.php">http://www.portghalibhospital.com/site/services_physicians.php</a> (<a href="port-ghalib-services.png">backup 2017-05-31</a>)</li>
                <li>Wypiska ze szpitala <a href="http://www.openstreetmap.org/search?query=port%20ghalib#map=12/25.5317/34.6338">Port Ghalib</a> (29.04.2017 18:58): <a href="img/wypiska1.jpg">1</a>, <a href="img/wypiska2.jpg">2</a>.<br /></li>
                <li>Wypiska ze szpitala Hurghada (30.04.2017 08:00-08:30): <a href="img/wypiska-3.jpg">3</a>, <a href="img/wypiska-4.jpg">4</a>.<br /></li>
                <li><a href="https://www.wykop.pl/wpis/23979533/chyba-nie-bylo-do-weryfikacji-z-fb-bylo-w-jednym-z/">https://www.wykop.pl/wpis/23979533/chyba-nie-bylo-do-weryfikacji-z-fb-bylo-w-jednym-z/</a> <i>(<a href="img-1.png">Screenshot zapisany dnia 2017-05-31</a>)</i></li>
                <li><a href="img/ogloszenia.jpg">Ogłoszenia sprzedaży mebli i samochodu</a></li>
                <li><a href="img/wpisy-fb.jpg">Wpisy Ahmeda na profilu fb Magdy</a></li>
                <li>Wpisy Macieja S. dotyczące palenia dziewic: <a href="img/sol-dziewice.jpg">1</a>, <a href="img/sol-dziewice-2.jpg">2</a></li>
                <li><a href="img/fes.jpg">Wpis KRS firmy Macieja S. (produkcja chemikaliów i wyrobów chemicznych)</a></li>
                <li><a href="img/ostatnie-logowanie.jpg">Ostatnie logowanie Magdy na Viberze</a></li>
                <li><a href="img/siostra.png">Siostra Markusa - Anja P.</a></li>
                <li><a href="img/mahmoud-solczak.jpg">Znajomość na fb pomiędzy Maciejem S. a Mahmoudem</a></li>
                <li><a href="http://www.fakt.pl/wydarzenia/polska/wroclaw/egipt-tajemnicza-smierc-magdaleny-zuk-w-egipcie/7p785fl">Pierwsze wstępne wyniki sekcji zwłok.</a></li>
                <li>Wylot magdy z KTW: w towarzystwie dwóch mężczyzn: <a href="http://www.tvp.info/30446642/magda-zuk-do-egiptu-wyruszyla-z-pyrzowic-prokuratura-bada-nagrania">KLIK</a></li>
                <li>Magda wg jednej z uczestniczek lotu płakała i była wzburzona już na lotnisku KTW: <a href="http://wiadomosci.gazeta.pl/wiadomosci/7,114871,21782423,nagranie-ze-szpitala-pokazuje-ze-magdalena-zuk-uciekala-wszystko.html">KLIK</a></li>
                </ul>
                <h3>Podziękowania:</h3>
                <a href="https://www.wykop.pl/ludzie/grejfrut/">@grejfrut</a>&nbsp;
                <a href="https://www.wykop.pl/ludzie/OstrzeSprawiedliwosci/">@OstrzeSprawiedliwosci</a>&nbsp;
                <a href="https://www.wykop.pl/ludzie/stopmanipulacji1111/">@stopmanipulacji1111</a>&nbsp;
                <a href="https://www.wykop.pl/ludzie/Unreal6277/">@Unreal6277</a>&nbsp;
                <a href="https://www.wykop.pl/ludzie/Syntax/">@Syntax</a>&nbsp;
            </div>
        );
    }
}

Main.propTypes = {
};
