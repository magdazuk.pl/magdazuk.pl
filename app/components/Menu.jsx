import React from "react";
import {Link} from "react-router";

export default class Menu extends React.Component {
    render() {
        return (
            <div>
            <ul>
                <br />
                <li><Link to="/">Informacje, linki</Link></li><br />
                <li>Timeline</li>
                <li>
                    <ul>
                        <li>&middot;<Link to="/timeline-before">&nbsp;przed wyjazdem</Link></li>
                        <li>&middot;<Link to="/timeline">&nbsp;w trakcie wyjazdu</Link></li>
                    </ul>
                </li><br />
                <li><Link to="/video">Nagranie rozmowy</Link></li><br />
                <li><Link to="/contact">Kontakt z organami</Link></li>
            </ul>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            </div>
        );
    }
}

Menu.propTypes = {
};
