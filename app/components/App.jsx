import React from "react";
import {Router, Route, browserHistory} from "react-router";
import {Row, Col} from "react-bootstrap";

import Menu from "./Menu.jsx";
import Main from "./Main.jsx";
import Timeline from "./Timeline.jsx";
import TimelineBefore from "./TimelineBefore.jsx";
import Video from "./Video.jsx";
import Contact from "./Contact.jsx";


class App extends React.Component {
    render() {
        return (
            <Row className="container">
                <Col md={12} className="header">
                    <img src="img/magda.png" />
                    <div>
                        <h1>Magdalena Żuk - tragiczna śmierć</h1>
                        <small>Na tej stronie zostaną zamieszczone różnorakie informacje oraz hipotezy dotyczące śmierci.</small>
                    </div>
                </Col>
                <Col md={2} className="menu">
                    <Menu />
                </Col>
                <Col md={10} className="content">
                    {this.props.children}
                </Col>
            </Row>
        );
    }
}

export default class Main_ extends React.Component {
    render() {
        return (
            <Router history={browserHistory} id="root">
                <Route component={App}>
                    <Route path="/" component={Main} />
                    <Route path="/timeline" component={Timeline} />
                    <Route path="/timeline-before" component={TimelineBefore} />
                    <Route path="/video" component={Video} />
                    <Route path="/contact" component={Contact} />
                </Route>
            </Router>
        );
    }
}

App.propTypes = {
    children: React.PropTypes.node,
};
