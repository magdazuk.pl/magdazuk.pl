import "./styles/main.scss";
import "./styles/timeline.css";
import React from "react"; // eslint-disable-line no-unused-vars
import ReactDOM from "react-dom";
import App from "./components/App.jsx"; // eslint-disable-line no-unused-vars
import Bootstrap from "bootstrap/dist/css/bootstrap.css"; // eslint-disable-line no-unused-vars

main();

function main() {
    const app = document.createElement("div");
    app.setAttribute("class", "magdazuk-app");
    document.body.appendChild(app);
    ReactDOM.render(<App />, app);
}
